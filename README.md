# README #

* Obtain from client, the creds for a Linode and for their DNS.
* Tell the Linode to publish the reverse DNS for its IP6 address.
  This is said to take up to 24h to have effect.
* Consider reducing, temporarily, the TTL on the DNS.
* Tell Linode to install Centos 6.

The difference between "script" files and "snippet" files in my usage of the terms here is that a script is designed to be run as a whole program, invoked with a command, but the idea with a "snippet" file is you open it in some browser or editor and paste in commands from it one at a time into a terminal. The idea is for you to judge whether the commands are really appropriate. Some of them may interact with you, also, which would make them hard to put into a script. Sometimes a reboot may be required.

* Bring up snippets/remote . Carry out
  the commands found there on any computer on the net, from a directory where
  you have a working copy of this repo.

* Something is missing right here.

* ssh in as the app user and follow snippets found in the "user_adm" repo, which
  from the above steps you will already have cloned into
  /home/adm/projects/user_adm .

* Alias postmaster mail address to somewhere other than root, e. g. adm?

* Arrange that a reboot of the system would start up Postfix.

* Arrange that a reboot of the system would restart the Unicorn app servers.
  There is a snippet file that may help.

* If you have modified anything in your clone of this repo and/or the
  "user_adm" repo, be sure to push up your improvements.
