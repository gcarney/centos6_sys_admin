# Snippets for setting up a bare system without necessarily any Ruby or web
# sites. For example the node for storing data backups might not need Ruby.

# Subgoal: populate .ssh dir for root, so can dispense with having to enter
# the password. Even though I prefer that logging as root should not be routine
# for administering the system, there are so many operations that have to be
# done in the root account just to set up the "adm" account, that setting up
# .ssh reduces the number of times the root password has to be entered manually
# from three or four times to one time, or so I hope.

set -u

# The file modes for the directory and its contents skel/app_user.ssh have to
# be correct for the next step to work by itself to make it unnecessary to
# cite the root pw again.

script/each 'scp -pr skel/app_user.ssh root@$host:.ssh'

# Did it work?

script/each 'ssh root@$host id'

# Set up the 'adm' user account for routine system administration.

script/each 'ssh root@$host <stuff/adm_home'
script/each 'ssh adm@$host <skel/populate_adm'
script/each 'ssh root@$host <skel/enable_adm_sudo'

# The next step will set up iptables and reboot the servers. Afterward, you
# have to wait for them to boot before sending more commands to them. So, to
# automate this step for a real fire-and-forget script could be less than
# straightforward.

script/each 'ssh adm@$host <skel/set_up_iptables_and_reboot'

script/each 'ssh adm@$host <skel/install_sys_software'

